
XML Form
-----------------
Provides a XML abstraction layer for Drupal Form API arrays
(https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7).

With this module, you can create and edit XML forms from admin interface (it
creates a new block type). It is also possible to use the module as a helper to
integrate with some external XML form generators.

XML form also integrates with Codemirror to make easier for editing the XML
estruture.

Note: This module does not save the form data. You need to build you own form
submit functions (please check the xml_form_example sub-module).

Requirements
-----------------
SimpleXML extension: http://www.php.net/manual/en/book.simplexml.php
Libraries module: http://drupal.org/project/libraries
Codemirror: http://codemirror.net

Installation
-----------------
- Install Libraries module
- Download the CodeMirror package and unzip in to your libraries folder:
http://codemirror.net/codemirror.zip.
- Rename the folder to "codemirror".
- Make sure codemirror.js is located in the following location:
libraries/codemirror/lib/codemirror.js.
- Install XML form module.

Usage
-----------------
Create a new XML form block (Structure -> Blocks -> Add XML Form block) and
copy the following example on the XML Form content field:
<?xml version="1.0" encoding="UTF-8"?>
<form form_id="form_test_1">
  <fields>
    <field id="active" type="radios">
      <title>Poll status</title>
      <options>
        <closed>Closed</closed>
        <active>Active</active>
      </options>
      <description>When a poll is closed, visitors can no longer vote for it.</description>
      <default_value>closed</default_value>
    </field>

    <field id="title" required="true" type="textfield" size="60" maxlength="128">
      <title>Subject</title>
    </field>
  </fields>
</form>

Save and place the block in a desirable region. The form now will show on our
site.

For customizing the form, create the following hook_xml_form_info() in your
custom module and implement the functions you listed:

<?php
/**
 * Implements hook_xml_form_info().
 */
function xml_form_example_xml_form_info() {
  return array(
    'form_test_1' => array(
      'alter' => array('xml_form_form_test_1_alter'),
      'validate' => array('xml_form_form_test_1_validate'),
      'submit' => array('xml_form_form_test_1_submit'),
      'theme' => 'xml_form_form_test_1',
    ),
  );
}
?>

Please see the sub-module xml_form_example for more details.

Available field types
-----------------
This module uses some field types from Form API, but this list will be reviewed
to support all Form API elements and some other custom elements. Theoretically
you can configure the elements with all available options in Form API
documentation by using them as properties in the <field> node or as child nodes.

actions
button
checkbox
checkboxes
date
fieldset
file
hidden
image_button
item
markup
password
password_confirm
radio
radios
select
submit
textarea
textfield
value

Roadmap
-----------------
There are some items in the roadmap for this module development:

- Support for validations configuration directly on XML (and hooks for custom
validations functions)
- Configuration of dependent fields
- Masks and placeholders
- Error messages grouped at the top of the form instead of default Drupal
message region
- Move to entity structure (stop using block)
- Better support for new fields (no need the creation of new class if the
element does not need customization to work)

Contact
-----------------
Current maintainers:
Rodrigo Goncalves (rodrigoeg) - https://www.drupal.org/u/rodrigoeg
