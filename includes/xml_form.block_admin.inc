<?php

/**
 * @file
 * Block admin page callbacks for the xml form module.
 */

/**
 * Form constructor for the add xml form block form.
 *
 * @see block_add_block_form_validate()
 * @see block_add_block_form_submit()
 *
 * @ingroup forms
 */
function xml_form_add_block_form(array $form, &$form_state) {
  return xml_form_admin_configure($form, $form_state, 'xml_form', NULL);
}

/**
 * Form validation handler for xml_form_add_block_form().
 *
 * @see xml_form_add_block_form()
 * @see xml_form_add_block_form_submit()
 */
function xml_form_add_block_form_validate($form, &$form_state) {
  $custom_block_exists = (bool) db_query_range('SELECT 1 FROM {block_xml_form} WHERE info = :info', 0, 1, array(':info' => $form_state['values']['info']))->fetchField();

  if (empty($form_state['values']['info']) || $custom_block_exists) {
    form_set_error('info', t('Ensure that each block description is unique.'));
  }
}

/**
 * Form submission handler for block_add_block_form().
 *
 * Saves the new custom block.
 *
 * @see xml_form_add_block_form()
 * @see xml_form_add_block_form_validate()
 */
function xml_form_add_block_form_submit($form, &$form_state) {
  $delta = db_insert('block_xml_form')
    ->fields(array(
      'info' => $form_state['values']['info'],
      'xml_content' => $form_state['values']['xml_content'],
    ))
    ->execute();
  // Store block delta to allow other modules to work with new block.
  $form_state['values']['delta'] = $delta;

  $query = db_insert('block')->fields(array(
    'visibility',
    'pages',
    'custom',
    'title',
    'module',
    'theme',
    'status',
    'weight',
    'delta',
    'cache',
  ));

  foreach (list_themes() as $theme) {
    if ($theme->status) {
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'status' => 0,
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  // Store regions per theme for this block.
  foreach ($form_state['values']['regions'] as $theme => $region) {
    db_merge('block')
      ->key(array(
        'theme' => $theme,
        'delta' => $delta,
        'module' => $form_state['values']['module'],
      ))
      ->fields(array(
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'pages' => trim($form_state['values']['pages']),
        'status' => (int) ($region != BLOCK_REGION_NONE),
      ))
      ->execute();
  }

  drupal_set_message(t('The block has been created.'));

  cache_clear_all();

  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Form constructor for the xml form block configuration form.
 *
 * Also used by xml_form_add_block_form() for adding a new custom xml form
 * block.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array with the previously submeted values.
 * @param string $module
 *   Name of the module that implements the block to be configured.
 * @param int $delta
 *   Unique ID of the block within the context of $module.
 *
 * @see block_admin_configure_validate()
 * @see block_admin_configure_submit()
 *
 * @ingroup forms
 */
function xml_form_admin_configure(array $form, array &$form_state, $module, $delta) {
  $block = block_load($module, $delta);
  $form['module'] = array(
    '#type' => 'value',
    '#value' => $block->module,
  );
  $form['delta'] = array(
    '#type' => 'value',
    '#value' => $block->delta,
  );

  // Get the block subject for the page title.
  $info = module_invoke($block->module, 'block_info');
  if (isset($info[$block->delta])) {
    drupal_set_title(t("'%name' block", array('%name' => $info[$block->delta]['info'])), PASS_THROUGH);
  }

  $form['settings']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block title'),
    '#maxlength' => 64,
    '#description' => $block->module == 'block' ? t('The title of the block as shown to the user.') : t('Override the default title for the block. Use <em>!placeholder</em> to display no title, or leave blank to use the default block title.', array('!placeholder' => '&lt;none&gt;')),
    '#default_value' => isset($block->title) ? $block->title : '',
    '#weight' => -19,
  );

  // Module-specific block configuration.
  $settings = module_invoke($block->module, 'block_configure', $block->delta);
  if ($settings) {
    foreach ($settings as $k => $v) {
      $form['settings'][$k] = $v;
    }
  }

  // Region settings.
  $form['regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Region settings'),
    '#collapsible' => FALSE,
    '#description' => t('Specify in which themes and regions this block is displayed.'),
    '#tree' => TRUE,
  );

  $theme_default = variable_get('theme_default', 'bartik');
  $admin_theme = variable_get('admin_theme');
  foreach (list_themes() as $key => $theme) {
    // Only display enabled themes.
    if ($theme->status) {
      $region = db_query("SELECT region FROM {block} WHERE module = :module AND delta = :delta AND theme = :theme", array(
        ':module' => $block->module,
        ':delta' => $block->delta,
        ':theme' => $key,
      ))->fetchField();

      // Use a meaningful title for the main site theme and administrative
      // theme.
      $theme_title = $theme->info['name'];
      if ($key == $theme_default) {
        $theme_title = t('!theme (default theme)', array('!theme' => $theme_title));
      }
      elseif ($admin_theme && $key == $admin_theme) {
        $theme_title = t('!theme (administration theme)', array('!theme' => $theme_title));
      }
      $form['regions'][$key] = array(
        '#type' => 'select',
        '#title' => $theme_title,
        '#default_value' => !empty($region) && $region != -1 ? $region : NULL,
        '#empty_value' => BLOCK_REGION_NONE,
        '#options' => system_region_list($key, REGIONS_VISIBLE),
        '#weight' => ($key == $theme_default ? 9 : 10),
      );
    }
  }

  // Visibility settings.
  $form['visibility_title'] = array(
    '#type' => 'item',
    '#title' => t('Visibility settings'),
  );
  $form['visibility'] = array(
    '#type' => 'vertical_tabs',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'block') . '/block.js'),
    ),
  );

  // Per-path visibility.
  $form['visibility']['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
    '#weight' => 0,
  );

  $access = user_access('use PHP for settings');
  if (isset($block->visibility) && $block->visibility == BLOCK_VISIBILITY_PHP && !$access) {
    $form['visibility']['path']['visibility'] = array(
      '#type' => 'value',
      '#value' => BLOCK_VISIBILITY_PHP,
    );
    $form['visibility']['path']['pages'] = array(
      '#type' => 'value',
      '#value' => isset($block->pages) ? $block->pages : '',
    );
  }
  else {
    $options = array(
      BLOCK_VISIBILITY_NOTLISTED => t('All pages except those listed'),
      BLOCK_VISIBILITY_LISTED => t('Only the listed pages'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ));

    if (module_exists('php') && $access) {
      $options += array(BLOCK_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['visibility']['path']['visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Show block on specific pages'),
      '#options' => $options,
      '#default_value' => isset($block->visibility) ? $block->visibility : BLOCK_VISIBILITY_NOTLISTED,
    );
    $form['visibility']['path']['pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $title . '</span>',
      '#default_value' => isset($block->pages) ? $block->pages : '',
      '#description' => $description,
    );
  }

  // Per-role visibility.
  $default_role_options = db_query("SELECT rid FROM {block_role} WHERE module = :module AND delta = :delta", array(
    ':module' => $block->module,
    ':delta' => $block->delta,
  ))->fetchCol();
  $role_options = array_map('check_plain', user_roles());
  $form['visibility']['role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
    '#weight' => 10,
  );
  $form['visibility']['role']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show block for specific roles'),
    '#default_value' => $default_role_options,
    '#options' => $role_options,
    '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
  );

  // Per-user visibility.
  $form['visibility']['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'visibility',
    '#weight' => 20,
  );
  $form['visibility']['user']['custom'] = array(
    '#type' => 'radios',
    '#title' => t('Customizable per user'),
    '#options' => array(
      BLOCK_CUSTOM_FIXED => t('Not customizable'),
      BLOCK_CUSTOM_ENABLED => t('Customizable, visible by default'),
      BLOCK_CUSTOM_DISABLED => t('Customizable, hidden by default'),
    ),
    '#description' => t('Allow individual users to customize the visibility of this block in their account settings.'),
    '#default_value' => isset($block->custom) ? $block->custom : BLOCK_CUSTOM_FIXED,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save block'),
  );

  return $form;
}

/**
 * Form validation handler for xml_form_admin_configure().
 *
 * @see xml_form_admin_configure()
 * @see xml_form_admin_configure_submit()
 */
function xml_form_admin_configure_validate(array $form, array &$form_state) {
  if ($form_state['values']['module'] == 'xml_form') {
    $custom_block_exists = (bool) db_query_range('SELECT 1 FROM {block_xml_form} WHERE bid <> :bid AND info = :info', 0, 1, array(
      ':bid' => $form_state['values']['delta'],
      ':info' => $form_state['values']['info'],
    ))->fetchField();
    if (empty($form_state['values']['info']) || $custom_block_exists) {
      form_set_error('info', t('Ensure that each block description is unique.'));
    }
  }
}

/**
 * Form submission handler for xml_form_admin_configure().
 *
 * @see xml_form_admin_configure()
 * @see xml_form_admin_configure_validate()
 */
function xml_form_admin_configure_submit($form, &$form_state) {
  if (!form_get_errors()) {
    $transaction = db_transaction();
    try {
      db_update('block')
        ->fields(array(
          'visibility' => (int) $form_state['values']['visibility'],
          'pages' => trim($form_state['values']['pages']),
          'custom' => (int) $form_state['values']['custom'],
          'title' => $form_state['values']['title'],
        ))
        ->condition('module', $form_state['values']['module'])
        ->condition('delta', $form_state['values']['delta'])
        ->execute();

      db_delete('block_role')
        ->condition('module', $form_state['values']['module'])
        ->condition('delta', $form_state['values']['delta'])
        ->execute();
      $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
      foreach (array_filter($form_state['values']['roles']) as $rid) {
        $query->values(array(
          'rid' => $rid,
          'module' => $form_state['values']['module'],
          'delta' => $form_state['values']['delta'],
        ));
      }
      $query->execute();

      // Store regions per theme for this block.
      foreach ($form_state['values']['regions'] as $theme => $region) {
        db_merge('block')
          ->key(array(
            'theme' => $theme,
            'delta' => $form_state['values']['delta'],
            'module' => $form_state['values']['module'],
          ))
          ->fields(array(
            'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
            'pages' => trim($form_state['values']['pages']),
            'status' => (int) ($region != BLOCK_REGION_NONE),
          ))
          ->execute();
      }

      module_invoke($form_state['values']['module'], 'block_save', $form_state['values']['delta'], $form_state['values']);
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('block', $e);
      throw $e;
    }
    drupal_set_message(t('The block configuration has been saved.'));

    cache_clear_all('xml_form:' . $form_state['values']['delta'], 'cache_xml_form');
    cache_clear_all();

    $form_state['redirect'] = 'admin/structure/block';
  }
}

/**
 * Form constructor for the custom block deletion form.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array with the previously submeted values.
 * @param string $module
 *   The name of the module that implements the block to be deleted. This should
 *   always equal 'block' since it only allows custom blocks to be deleted.
 * @param int $delta
 *   The unique ID of the block within the context of $module.
 *
 * @see xml_form_custom_block_delete_submit()
 */
function xml_form_custom_block_delete(array $form, array &$form_state, $module, $delta) {
  $block = block_load($module, $delta);
  $custom_block = xml_form_custom_block_get($block->delta);
  $form['info'] = array('#type' => 'hidden', '#value' => $custom_block['info'] ? $custom_block['info'] : $custom_block['title']);
  $form['bid'] = array('#type' => 'hidden', '#value' => $block->delta);

  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $custom_block['info'])), 'admin/structure/block', '', t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for xml_form_block_delete().
 *
 * @see xml_form_custom_block_delete()
 */
function xml_form_custom_block_delete_submit($form, &$form_state) {
  db_delete('block_xml_form')
    ->condition('bid', $form_state['values']['bid'])
    ->execute();
  db_delete('block')
    ->condition('module', 'xml_form')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  db_delete('block_role')
    ->condition('module', 'xml_form')
    ->condition('delta', $form_state['values']['bid'])
    ->exxml_form_add_block_formecute();

  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));

  cache_clear_all('xml_form:' . $form_state['values']['bid'], 'cache_xml_form');
  cache_clear_all();

  $form_state['redirect'] = 'admin/structure/block';
}
