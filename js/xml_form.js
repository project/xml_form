/**
 * @file
 * Javascript that adds codemirror into textarea for bether UI.
 */

(function ($) {

  "use strict";

  /**
   * Codemirror configuration for xml form block textarea.
   *
   * Behaviors that insert the code mirror editor to the xml form block
   * configuration xml configuration text area.
   */
  Drupal.behaviors.xml_form_codemirror = {
    attach: function (context, settings) {
      $("#edit-xml-content", context).once("onload", function () {
        var text_area = document.getElementById("edit-xml-content");

        if (typeof text_area === "undefined" && typeof CodeMirror === "undefined") {
          return;
        }

        CodeMirror.fromTextArea(text_area, {
          lineNumbers: true,
          lineWrapping: true,
          matchBrackets: true,
          mode: {name: "xml", alignCDATA: true},
          indentUnit: 2,
          indentWithTabs: false,
          enterMode: "keep",
          tabMode: "shift"
        });
      });
    }
  };

})(jQuery);
