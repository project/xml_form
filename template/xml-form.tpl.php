<?php

/**
 * @file
 * The theme of XML form.
 *
 * Available variables:
 * - $form: The form array.
 */
?>
<?php print drupal_render($form); ?>
