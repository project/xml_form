<?php

/**
 * @file
 * Xml form function that converts XML into a form.
 */

/**
 * Builds the Drupal form field.
 */
class XmlFormFieldTextfield extends XmlFormField {

  /**
   * Responsible to build the field array.
   */
  protected function buildField() {
    $this->field = array(
      '#type' => 'textfield',
    );
  }

}
