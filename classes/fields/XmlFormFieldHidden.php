<?php

/**
 * @file
 * Xml form function that converts XML into a form.
 */

/**
 * Builds the Drupal form field.
 */
class XmlFormFieldHidden extends XmlFormField {

  /**
   * Responsible to build the field array.
   */
  protected function buildField() {
    $this->field = array(
      '#type' => 'Hidden',
    );
  }

}
