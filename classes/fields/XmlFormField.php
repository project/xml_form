<?php

/**
 * @file
 * Xml form function that converts XML into a Drupal form field.
 */

/**
 * Builds the Drupal form field.
 */
class XmlFormField {

  protected $id = '';
  protected $options = array();
  protected $field = array();

  /**
   * Builds the field array.
   *
   * @param string $id
   *   The id of the field.
   * @param array $options
   *   Options (attributes and children) from the XML.
   */
  public function __construct($id, array $options) {
    $this->id = $id;
    $this->options = $options;
    $this->buildField();
    $this->buildCommonAttributes();
  }

  /**
   * Get the field array.
   *
   * @return array
   *   With the field structure.
   */
  public function getField() {
    return $this->field;
  }

  /**
   * Responsible to build the field array.
   */
  protected function buildField() {
    // Overrride this.
  }

  /**
   * Set all the attributes of the field that are common for all the fields.
   */
  protected function buildCommonAttributes() {
    foreach ($this->options as $key => $value) {
      $this->field['#' . $key] = $value;
    }
  }

}
