<?php

/**
 * @file
 * Xml form function that converts XML into a form.
 */

/**
 * Responsible to build all the FAPI elements from a XML form.
 */
class XmlFormBuild {

  protected $xml = array();
  protected $form = array();
  protected $weight = 1;
  protected $delta = 0;

  /**
   * Constructor of the form.
   *
   * @param int $delta
   *   The delta of the form block.
   * @param string $xml_string
   *   String containing the form in the XML format.
   */
  public function __construct($delta, $xml_string) {

    $this->delta = $delta;

    $cache = $this->getCache();
    if ($cache) {
      $this->form = $cache;
      return;
    }

    $xml_object = simplexml_load_string($xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);
    $this->xml = json_decode(json_encode((array) $xml_object), TRUE);
    $this->buildForm();

    $this->setCache();
  }

  /**
   * Get the cache from cache_xml_form table.
   *
   * @return array
   *   With the cache information
   */
  protected function getCache() {
    $cache = cache_get("xml_form:" . $this->delta, 'cache_xml_form');
    if ($cache && !empty($cache->data)) {
      return $cache->data;
    }
    return NULL;
  }

  /**
   * Set the form already built in the cache_xml_form table.
   */
  protected function setCache() {
    cache_set("xml_form:" . $this->delta, $this->form, 'cache_xml_form', CACHE_TEMPORARY);
  }

  /**
   * Get the form array already built.
   *
   * @return array
   *   With the form in the default Drupal format
   */
  public function getForm() {
    return $this->form;
  }

  /**
   * Get the form id configured in the xml with form_id property.
   *
   * Example:
   *   <form_definitions form_id="form_test_1">.
   *
   * @return string
   *   Value of the form id defined.
   */
  protected function getFormId() {
    return $this->xml['@attributes']['form_id'];
  }

  /**
   * Builds the form from XML to Drupal format.
   */
  protected function buildForm() {
    $this->callXmlFormInfoHook();

    if (isset($this->xml['fields']['field'])) {
      $fields = $this->xml['fields']['field'];
      if (!is_array($fields)) {
        $fields = array($fields);
      }

      foreach ($fields as $field) {
        $options = array();

        foreach ($field['@attributes'] as $attribute_key => $attribute_value) {
          $options[$attribute_key] = $attribute_value;
        }

        foreach ($field as $field_option_key => $field_option_value) {
          if ($field_option_key != '@attributes') {
            $options[$field_option_key] = $field_option_value;
          }
        }

        $id_with_parents = $options['id'];
        $options['id'] = $this->getFieldId($options['id']);

        $this->addField($options['id'], $options['type'], $options, $this->getFieldParent($id_with_parents));
      }
    }
  }

  /**
   * Get the field id without the parents.
   *
   * Examples:
   *   input: [parent]field_id.
   *   output: field_id.
   *
   * @param string $field_id
   *   The field id of the field (with the parents).
   *
   * @return string
   *   The field id without the parents.
   */
  protected function getFieldId($field_id) {
    $parent_array = explode(']', $field_id);
    return array_pop($parent_array);
  }

  /**
   * Get the reference of the parent field.
   *
   * @param string $field_id
   *   The field id of the field (with the parents).
   *
   * @return array
   *   The parent of the field.
   */
  protected function &getFieldParent($field_id) {
    $parent_array = explode(']', $field_id);
    array_pop($parent_array);
    if (count($parent_array) > 0) {
      foreach ($parent_array as $count => $parent) {
        $parent_array[$count] = str_replace('[', '', $parent);
      }

      $key_exists = NULL;
      $value = &drupal_array_get_nested_value($this->form, $parent_array, $key_exists);
      if ($key_exists) {
        return $value;
      }
    }
    else {
      return $this->form;
    }
  }

  /**
   * Call the hook_xml_form_info().
   */
  protected function callXmlFormInfoHook() {
    $forms_info = module_invoke_all('xml_form_info');
    if (isset($forms_info[$this->getFormId()])) {
      $form_info = $forms_info[$this->getFormId()];

      // TODO check if there is some property already on the form.
      if (isset($form_info['default_value'])) {
        $this->form['#default_value_function'] = $form_info['default_value'];
      }

      if (isset($form_info['alter'])) {
        $this->form['#alter'] = $form_info['alter'];
      }

      if (isset($form_info['validate'])) {
        $this->form['#validate'] = $form_info['validate'];
      }

      if (isset($form_info['submit'])) {
        $this->form['#submit'] = $form_info['submit'];
      }

      if (isset($form_info['theme'])) {
        $this->form['#theme'] = $form_info['theme'];
      }
    }
  }

  /**
   * Add a field into the form array.
   *
   * @param string $id
   *   The id of the field.
   * @param string $type
   *   The type of the field.
   * @param array $options
   *   With all the options (attributes and children) of a field from the XML.
   * @param array $parent
   *   With all the parents name.
   */
  protected function addField($id, $type, array $options, array &$parent) {
    $type_array = explode('_', $type);
    $type = '';
    foreach ($type_array as $type_part) {
      $type .= ucfirst($type_part);
    }

    $class_name = 'XmlFormField' . $type;
    if (class_exists($class_name)) {
      $field = new $class_name($id, $options);
      $parent[$id] = $field->getField();
      $parent[$id]['#weight'] = $this->weight++;

      if (isset($this->form['#default_value_function']) && function_exists($this->form['#default_value_function'])) {
        $parent[$id]['#default_value'] = $this->form['#default_value_function']($this->form, $parent[$id]);
      }
    }
  }

}
