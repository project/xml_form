<?php

/**
 * @file
 * Hooks provided by the XML Form module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines the functions to be executed for a XML form.
 *
 * Modules may change de default_values, alter, validate, submit and create a
 * custom theme.
 */
function hook_xml_form_info() {
  return array(
    'form_test_1' => array(
      // 'default_value' => 'xml_form_form_test_1_default_value',
      'alter' => array('xml_form_form_test_1_alter'),
      'validate' => array('xml_form_form_test_1_validate'),
      'submit' => array('xml_form_form_test_1_submit'),
      'theme' => 'xml_form_form_test_1',
    ),
  );
}
/**
 * @} End of "addtogroup hooks".
 */
